package com.polina.retrofit6.retrifiteample

data class AuthRequest(
    val username: String,
    val password: String
)
