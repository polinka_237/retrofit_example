package com.polina.retrofit6.retrifiteample

import retrofit2.Response
import retrofit2.http.*

//здесь будут прописываться все запросы, которые retrofit будет отправлять
interface MainApi {
    @GET("auth/products/{id}")
    suspend fun getProductById(@Path("id") id: Int): Product

    @POST("auth/login")
    suspend fun auth(@Body authRequest: AuthRequest): Response<User>

//    @GET("auth/products")
//    suspend fun getAllProducts(): Products
    @Headers("Content-Type: application/json")
    @GET("auth/products")
    suspend fun getAllProducts(@Header("Authorization") token: String): Products

    @Headers("Content-Type: application/json")
    @GET("auth/products/search")
    suspend fun getProductsByNameAuth(@Header("Authorization") token: String,
                                      @Query("q") name: String): Products

}